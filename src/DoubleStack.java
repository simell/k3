import java.util.LinkedList;


public class DoubleStack {

  private LinkedList<Double> list;



   DoubleStack() {
      this.list = new LinkedList<Double>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack newStack = new DoubleStack();
        newStack.list = (LinkedList<Double>) list.clone();
      return newStack;
   }

   public boolean stEmpty() {
      return list.size() == 0;
   }

   public void push (double a) {
       list.add(a);
   }

   public double pop() {
       if (stEmpty()) {
           throw new RuntimeException("Can't pop from empty list!");
       } else {
           return list.removeLast();
       }
   }

   public void op (String s) {
       if (list.size() >= 2) {
           if (s.equals("+") || s.equals("-") || s.equals("*") || s.equals("/") || s.equals("SWAP") || s.equals("ROT")) {
               double s1 = list.removeLast();
               double s2 = list.removeLast();
               if (s.equals("*")) {
                   list.add(s2 * s1);
               } else if (s.equals("/")) {
                   list.add(s2 / s1);
               } else if (s.equals("+")) {
                   list.add(s2 + s1);
               } else if (s.equals("-")) {
                   list.add(s2 - s1);
               } else if (s.equals("SWAP")) {
                   list.add(s1);
                   list.add(s2);
               } else if (s.equals("ROT") && list.size() >= 1) {
                   double s3 = list.removeLast();
                   list.add(s2);
                   list.add(s1);
                   list.add(s3);
               }

           } else {
               throw new RuntimeException("The operation '" + s +  "' is not valid!");
           }
       } else {
           throw new RuntimeException("Not enough items in stack to complete operation '" + s + "'");
       }
   }

   public double tos() {
       if (stEmpty()) {
           throw new RuntimeException("Can't get last element from empty stack!");
       } else {
           return list.getLast();
       }
   }

   @Override
   public boolean equals (Object o) {
       return list.equals(((DoubleStack) o).list);
   }

   @Override
   public String toString() {
       StringBuilder str = new StringBuilder();
       for (Double aDouble : list) {
            str.append(aDouble.toString());
            str.append(" ");
       }
      return str.toString();
   }

   public static double interpret (String pol) {
       if (pol == null) {
           throw new RuntimeException("No equation was entered!");
       }
       DoubleStack stack = new DoubleStack();
       String equationString = pol.trim();
       String[] items = equationString.split(" ");
       if (items.length == 1 && items[0].equals("")) {
           throw new RuntimeException("The equation: '" + pol + "' is empty!");
       }
       int numbersCount = 0;
       int operationsCount = 0;

       for (String item : items) {
           if (item.equals("+") || item.equals("-") || item.equals("*") || item.equals("/") || item.equals("SWAP") || item.equals("ROT")){
               if (stack.list.size() < 2) {
                   throw new RuntimeException("Not enough numbers in the stack to complete operation '" +
                                                item + "' in equation '" + pol + "'!");
               } else {
                   // Double s1 = stack.list.removeLast();
                   // Double s2 = stack.list.removeLast();
                   stack.op(item);
                   operationsCount++;
                   if (item.equals("ROT") || item.equals("SWAP")){
                       operationsCount--;
                   }
                   /*if (item.equals("*")) {
                       stack.list.add(s2 * s1);
                   } else if (item.equals("/")) {
                       stack.list.add(s2 / s1);
                   } else if (item.equals("+")) {
                       stack.list.add(s2 + s1);
                   } else if (item.equals("-")){
                       stack.list.add(s2 - s1);
                   } else if (item.equals("SWAP")) {
                       stack.list.add(s1);
                       stack.list.add(s2);
                   }*/
               }

           } else if (!item.isEmpty() && isDouble(item)) {
               Double stackItem = Double.valueOf(item);
               stack.list.add(stackItem);
               numbersCount++;
           } else if (!isDouble(item) && !item.equals("")) {
               throw new RuntimeException("Equation '" + pol + "' contains invalid element '"+ item +"'!");
           }
       }

       if (operationsCount != numbersCount - 1) {
           throw new RuntimeException("Too many numbers in equation '" + pol + "'!");
       }
       return stack.list.pop();
   }

    public static boolean isDouble(String str) {
       try {
            Double.parseDouble(str);
            return true;
       } catch (NumberFormatException e) {
            return false;
       }
    }

   public static void main (String[] argum) {
       DoubleStack test = new DoubleStack();
       test.push(1.);
       test.push(2.);
       test.push(3.);
       System.out.println(test.tos());
       test.op("+");
       System.out.println(test.tos());
       test.op("+");
       System.out.println(test.tos());
       String s = "  11. 12. x + +";
       //DoubleStack.interpret(s);
       String x = "  1 2 3 ROT  - +  ";
       System.out.println(DoubleStack.interpret (x));
       String a1 = "2 5 SWAP -";
       System.out.println(DoubleStack.interpret (a1));
       String a2 = "2 5 9 ROT - +";
       System.out.println(DoubleStack.interpret (a2));
       String a3 = "2 5 9 ROT + SWAP -";
       System.out.println(DoubleStack.interpret (a3));


   }
}

